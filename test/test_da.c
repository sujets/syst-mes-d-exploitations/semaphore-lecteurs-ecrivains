#include <check.h>   // for Suite, srunner_create, srunner_free, srunner_...
#include <stdlib.h>  // for size_t, EXIT_FAILURE, EXIT_SUCCESS

#include "dataArray.h"  // for DA_init, DA_insert_back, DA_length, DA_value

START_TEST(test_da_length) {
  DataArray da;
  DA_init(&da, 3);

  for (size_t i = 10; i >= 1; --i) {
    DA_insert_back(&da, i);
  }

  ck_assert_int_eq(DA_length(&da), 10);
}
END_TEST

START_TEST(test_da_rw) {
  DataArray da;
  DA_init(&da, 3);

  for (size_t i = 10; i >= 1; --i) {
    DA_insert_back(&da, i);
  }

  for (size_t i = 0; i < DA_length(&da); ++i) {
    ck_assert_int_eq(DA_value(&da, i), 10 - i);
  }
}
END_TEST

Suite* da_suite(void) {
  Suite* s;
  TCase* tc_core;

  s = suite_create("DataArray");

  /* Core test case */
  tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_da_length);
  tcase_add_test(tc_core, test_da_rw);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(void) {
  int number_failed;
  Suite* s;
  SRunner* sr;

  s = da_suite();
  sr = srunner_create(s);

  srunner_run_all(sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

// int main() {
//   DataArray da;

//   DA_init(&da, 3);

//   printf("Hello\n");

//   for (size_t i = 10; i >= 1; --i) {
//     DA_insert_back(&da, i);
//   }

//   for (size_t i = 0; i < DA_length(&da); ++i) {
//     printf("da at i:%ld is %ld\n", i, DA_value(&da, i));
//   }

//   printf("da length is %ld\n", DA_length(&da));

//   return EXIT_SUCCESS;
// }