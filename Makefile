#/// @file 
#/// @brief Generic Makefile.                                                 
#                                                                                                   
#/// @detail If you just add some library files used by the project.c program, you have nothing to change to compile them if sources are in the ./src directory. To add a new binary, just add the name of the main file in the TARGETS variable.             


##############
# Constantes #
##############

# Repertoires
SOURCE = ./src
TEST = ./test
BUILD = ./build
APPS = ./apps
BIN = ./bin
DOCPATH = ${SOURCE}/dox
DOCTARGET = ./doc
DIRLIST = ${SOURCE} ${BIN} ${TEST} ${BUILD} ${DOCTARGET}
#DEP = ${SOURCE}/depend
#DIRLIST = ${SOURCE} ${BIN} ${OPT} ${DEP}

# Commandes
CC = gcc

# Options
CFLAGS = -O0 -g -W -Wall -Wextra -Wconversion -Werror -mtune=native  -march=native  -std=c11 -D_DEFAULT_SOURCE 
LDFLAGS = -lm -W -Wall -pedantic -L. -lm  -pthread
LDTESTFLAGS = -lcheck -lsubunit -lrt ${LDFLAGS}
# Fichiers
DOX = ${wildcard ${DOCPATH}/*.dox} # Sources
SRC = ${wildcard ${SOURCE}/*.c} # Sources
TGTS = ${wildcard ${APPS}/*.c} # Sources
TESTTGTS = ${wildcard ${TEST}/*.c} # Sources
INT = ${wildcard ${SOURCE}/*.h} # Interfaces
OBJ = ${SRC:${SOURCE}/%.c=${BUILD}/%.o}

# Cibles
BINTGTS = ${TGTS:${APPS}/%.c=${BIN}/%}
BINTESTTGTS = ${TESTTGTS:${TEST}/%.c=${BIN}/%}

##########
# Regles #
##########

# ALL
all : Makefile ${BINTGTS} ${BINTESTTGTS}

# Test
test : Makefile ${BINTESTTGTS}


# CLEAN
clean :
	@echo
	@echo Cleaning : object files
	@echo --------
	@echo
	rm -f ${OBJ}

clean-doc :
	@echo
	@echo Cleaning : object files
	@echo --------
	@echo
	rm -fr ${DOCTARGET}

clean-emacs :
	@echo
	@echo Cleaning : emacs back-ups
	@echo --------
	@echo
	rm -f ${SOURCE}/*~
	rm -f ${SOURCE}/\#*\#
	rm -f *~
	rm -f \#*\#

clean-bin :
	@echo
	@echo Cleaning : binaries
	@echo --------
	@echo
	rm -f ${BINTGTS} ${BINTESTTGTS}

distclean : clean clean-emacs clean-bin


dirs : 
	@for dir in ${DIRLIST} ;\
	do \
	    echo Creating directory : $${dir} ;\
	    echo ------------------ ;\
	    if test -d $${dir} ;\
	    then \
		echo Directory already exists ;\
	    else mkdir -p $${dir} ;\
	    fi ;\
	    echo Done ;\
	    echo ;\
	done

# Binaires
${BIN}/% : ${APPS}/%.c ${OBJ}
	@echo
	@echo Linking bytecode : $@
	@echo ----------------
	@echo
	${CC} -o $@ $^ ${LDFLAGS} -I${SOURCE}
	@echo
	@echo Done
	@echo

# Test
${BIN}/% : ${TEST}/%.c ${OBJ}
	@echo
	@echo Linking bytecode : $@
	@echo ----------------
	@echo
	${CC} -o $@ $^ ${LDTESTFLAGS} -I${SOURCE}
	@echo
	@echo Done
	@echo

# Regles generiques
${BUILD}/%.o : ${SOURCE}/%.c ${SOURCE}/%.h
	@echo
	@echo Compiling $@
	@echo --------
	@echo
	@echo 
	$(CC) $(CFLAGS) -c $< -o $@ -I${SOURCE}

# Regles generiques
${BUILD}/%.o : ${APPS}/%.c 
	@echo
	@echo Compiling $@
	@echo --------
	@echo
	$(CC) $(CFLAGS) -c $< -o $@ -I${SOURCE}

# Regles generiques
${BUILD}/%.o : ${TEST}/%.c 
	@echo
	@echo Compiling $@
	@echo --------
	@echo
	$(CC) $(CFLAGS) -c $< -o $@ -I${SOURCE}


# Documentation 
doc : ${SRC} ${INT} ${DOX}
	doxygen; doxygen



#############################
# Inclusion et spécificités #
#############################

.PHONY : all test run clean clean-doc clean-emacs clean-bin distclean doc

