/********************************
 * Fichier semaphore.h
 ********************************/
#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

extern void erreurFin(const char* msg);

extern void initSem(int nbSem, char* nomFichier, int* pvinit);

extern void libereSem();

extern void P(int numSem);

extern void V(int numSem);

#endif
